﻿CREATE DATABASE  IF NOT EXISTS `ioidata` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ioidata`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: ioidata
-- ------------------------------------------------------
-- Server version	5.6.23-log


--
-- Table structure for table `cholestech`
-- 

-- Esta versión está en thigareda@hotmail.com
-- ahora en itzacatepec
-- ahora modificada en hotmail

DROP TABLE IF EXISTS `cholestech`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cholestech` (
  `fecha_lectura` datetime NOT NULL,
  `id_paciente` int(11) NOT NULL DEFAULT '0',
  `unidades` tinyint(1) unsigned DEFAULT NULL,
  `TC` float DEFAULT NULL,
  `HDL` float DEFAULT NULL,
  `TRG` float DEFAULT NULL,
  `LDL` float DEFAULT NULL,
  `non_LDL` float DEFAULT NULL,
  `LDL_HDL` float DEFAULT NULL,
  `GLU` float DEFAULT NULL,
  `fuma` tinyint(1) DEFAULT NULL,
  `TA` tinyint(1) DEFAULT NULL,
  `PAS` tinyint(1) unsigned DEFAULT NULL,
  `Riesgo` tinyint(1) DEFAULT NULL,
  `version_firmware` varchar(10) DEFAULT NULL,
  `TC_HDL` float DEFAULT NULL,
  PRIMARY KEY (`fecha_lectura`,`id_paciente`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `cholestech_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacdoc` (`id_PacDoc`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cholestech`
--

LOCK TABLES `cholestech` WRITE;
/*!40000 ALTER TABLE `cholestech` DISABLE KEYS */;
INSERT INTO `cholestech` VALUES ('0001-01-01 00:00:00',1,1,4.89,0.92,1.73,3.17,3.97,0,5.46,1,1,0,6,'',5.3),('0001-01-01 00:00:00',5,1,3.56,1.16,0.72,2.06,2.4,0,4.37,0,0,1,0,'',3.1),('0001-01-01 00:00:00',9,0,104,24,74,65,80,2.7,98,0,0,0,0,'3.41',0),('0001-01-01 00:00:00',10,0,152,41,97,92,111,2.2,84,1,0,1,1,'',0),('2015-05-27 09:50:00',1,0,167,25,151,112,142,4.5,82,0,0,0,0,'3.41',0),('2015-05-27 09:50:00',4,0,167,25,151,112,142,4.5,82,0,0,0,0,'3.41',0),('2015-05-27 09:50:00',6,0,167,25,151,112,142,4.5,82,0,0,0,0,'3.41',0),('2015-06-05 11:33:00',4,0,5.62,1.66,2.66,2.74,3.96,0,4.65,0,0,0,0,'3.41',3.4),('2015-06-18 00:15:00',5,0,150,42,85,91,108,2.2,86,0,0,0,0,'3.41',0),('2015-06-25 11:14:00',5,1,3.56,1.16,0.72,2.06,2.4,0,4.37,1,1,3,25,'3.41',3.1),('2015-06-26 00:13:00',12,0,152,30,112,100,122,3.3,67,0,1,0,0,'3.41',0);
/*!40000 ALTER TABLE `cholestech` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ioi353`
--

DROP TABLE IF EXISTS `ioi353`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ioi353` (
  `val_FechaMuestra` datetime NOT NULL,
  `id_paciente` int(11) NOT NULL DEFAULT '0',
  `val_ID` char(30) DEFAULT NULL,
  `val_Height` char(5) DEFAULT NULL,
  `val_Weight` char(5) DEFAULT NULL,
  `val_Weight_Low` char(5) DEFAULT NULL,
  `val_StandarWeight` char(5) DEFAULT NULL,
  `val_Weight_High` char(5) DEFAULT NULL,
  `val_GoalToControl` char(5) DEFAULT NULL,
  `val_PBF` char(4) DEFAULT NULL,
  `val_PBF_Low` char(4) DEFAULT NULL,
  `val_PBF_High` char(4) DEFAULT NULL,
  `val_MBF` char(4) DEFAULT NULL,
  `val_MBF_Low` char(4) DEFAULT NULL,
  `val_MBF_High` char(4) DEFAULT NULL,
  `val_LeftArm_MBF` char(5) DEFAULT NULL,
  `val_RightArm_MBF` char(5) DEFAULT NULL,
  `val_LeftLeg_MBF` char(5) DEFAULT NULL,
  `val_RightLeg_MBF` char(5) DEFAULT NULL,
  `val_Trunk_MBF` char(6) DEFAULT NULL,
  `val_SegmentalAssessment_MBF` char(6) DEFAULT NULL,
  `val_LBM` char(5) DEFAULT NULL,
  `val_LBM_Low` char(5) DEFAULT NULL,
  `val_LBM_High` char(5) DEFAULT NULL,
  `val_TBW` char(5) DEFAULT NULL,
  `val_TBW_Low` char(5) DEFAULT NULL,
  `val_TBW_High` char(5) DEFAULT NULL,
  `val_BMI` char(5) DEFAULT NULL,
  `val_BMI_Low` char(5) DEFAULT NULL,
  `val_BMI_High` char(5) DEFAULT NULL,
  `val_Fatness` char(6) DEFAULT NULL,
  `val_BMR` char(5) DEFAULT NULL,
  `val_Impedance` char(5) DEFAULT NULL,
  `val_Protein` char(5) DEFAULT NULL,
  `val_Protein_Low` char(5) DEFAULT NULL,
  `val_Protein_High` char(5) DEFAULT NULL,
  `val_Mineral` char(5) DEFAULT NULL,
  `val_Mineral_Low` char(5) DEFAULT NULL,
  `val_Mineral_High` char(5) DEFAULT NULL,
  `val_SLM` char(5) DEFAULT NULL,
  `val_SLM_Low` char(5) DEFAULT NULL,
  `val_SLM_High` char(5) DEFAULT NULL,
  `val_LeftArm_SLM` char(5) DEFAULT NULL,
  `val_RightArm_SLM` char(5) DEFAULT NULL,
  `val_LeftLeg_SLM` char(5) DEFAULT NULL,
  `val_RightLeg_SLM` char(5) DEFAULT NULL,
  `val_Trunk_SLM` char(6) DEFAULT NULL,
  `val_SegmentalAssessment_SLM` char(6) DEFAULT NULL,
  `val_WHR` char(4) DEFAULT NULL,
  `val_WHR_High` char(4) DEFAULT NULL,
  `val_WHR_Low` char(4) DEFAULT NULL,
  `val_Level` char(3) DEFAULT NULL,
  `val_AMB` char(4) DEFAULT NULL,
  `val_TEE` char(5) DEFAULT NULL,
  `val_VFA` char(4) DEFAULT NULL,
  `val_DurationControl` char(4) DEFAULT NULL,
  `val_AC` char(5) DEFAULT NULL,
  PRIMARY KEY (`val_FechaMuestra`,`id_paciente`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `ioi353_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacdoc` (`id_PacDoc`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ioi353`
--

LOCK TABLES `ioi353` WRITE;
/*!40000 ALTER TABLE `ioi353` DISABLE KEYS */;
INSERT INTO `ioi353` VALUES ('2002-03-15 16:23:00',2,NULL,'158.9','63.8','49.9','55.5','61.0','8.3','26.8','15.0','20.0','17.1','8.3','11.1','1.12','1.12','3.07','3.05','8.74','33333','46.7','44.4','47.2','33.6','31.9','33.9','25.2','18.5','25.0','14.9','1357','487','9.4','8.8','9.9','3.7','3.2','3.3','43.0','41.1','43.8','2.75','2.75','7.64','7.71','22.15','22223','0.85','0.90','0.75','9','26','2090','86','15','88.7'),('2002-03-15 16:23:00',4,NULL,'158.9','63.8','49.9','55.5','61.0','8.3','26.8','15.0','20.0','17.1','8.3','11.1','1.12','1.12','3.07','3.05','8.74','33333','46.7','44.4','47.2','33.6','31.9','33.9','25.2','18.5','25.0','14.9','1357','487','9.4','8.8','9.9','3.7','3.2','3.3','43.0','41.1','43.8','2.75','2.75','7.64','7.71','22.15','22223','0.85','0.90','0.75','9','26','2090','86','15','88.7'),('2009-03-15 11:06:00',2,NULL,'154.3','59.0','47.1','52.4','57.6','6.6','32.9','20.0','30.0','19.4','10.5','15.7','1.26','1.22','3.49','3.47','9.95','33333','39.6','36.7','41.9','28.5','26.4','30.1','24.7','18.5','25.0','12.5','1132','516','7.7','7.3','8.3','3.4','2.8','3.1','36.2','33.7','38.7','2.30','2.33','6.50','6.57','18.50','22222','0.83','0.85','0.70','10','42','1743','76','13','78.7'),('2009-03-15 11:06:00',3,NULL,'154.3','59.0','47.1','52.4','57.6','6.6','32.9','20.0','30.0','19.4','10.5','15.7','1.26','1.22','3.49','3.47','9.95','33333','39.6','36.7','41.9','28.5','26.4','30.1','24.7','18.5','25.0','12.5','1132','516','7.7','7.3','8.3','3.4','2.8','3.1','36.2','33.7','38.7','2.30','2.33','6.50','6.57','18.50','22222','0.83','0.85','0.70','10','42','1743','76','13','78.7'),('2009-03-15 12:03:00',2,NULL,'155.1','48.1','47.6','52.9','58.1','-4.8','27.7','20.0','30.0','13.3','10.6','15.9','0.89','0.90','2.34','2.38','6.78','33222','34.8','37.0','42.3','25.1','26.6','30.4','19.9','18.5','25.0','9.0','1178','700','7.0','7.4','8.4','2.7','2.9','3.1','32.1','34.1','39.1','1.87','1.84','5.52','5.40','17.47','11112','0.77','0.85','0.70','5','21','1814','40','0','71.5'),('2019-02-15 19:25:00',2,NULL,'172.1','86.0','58.6','65.2','71.7','20.8','29.9','15.0','20.0','25.7','9.8','13.0','1.62','1.56','4.65','4.64','13.23','33333','60.3','52.2','55.4','43.4','37.5','39.8','29.0','18.5','25.0','31.9','1625','404','12.0','10.4','11.7','4.9','3.7','3.9','55.4','48.3','51.5','3.77','3.82','10.19','10.26','27.35','33333','0.88','0.90','0.75','11','26','2503','110','29','100.9'),('2019-02-15 19:25:00',10,NULL,'172.1','86.0','58.6','65.2','71.7','20.8','29.9','15.0','20.0','25.7','9.8','13.0','1.62','1.56','4.65','4.64','13.23','33333','60.3','52.2','55.4','43.4','37.5','39.8','29.0','18.5','25.0','31.9','1625','404','12.0','10.4','11.7','4.9','3.7','3.9','55.4','48.3','51.5','3.77','3.82','10.19','10.26','27.35','33333','0.88','0.90','0.75','11','26','2503','110','29','100.9'),('2025-06-15 13:35:00',1,NULL,'170.0','81.0','57.2','63.6','69.9','17.4','30.5','15.0','20.0','24.7','9.5','12.7','1.59','1.54','4.45','4.44','12.68','33333','56.3','50.9','54.1','40.5','36.6','38.9','28.0','18.5','25.0','27.3','1545','453','11.2','10.1','11.4','4.6','3.6','3.8','51.7','47.1','50.2','3.42','3.46','9.37','9.38','26.07','33333','0.89','0.90','0.75','12','28','2379','116','27','99.5'),('2025-06-15 13:43:00',9,NULL,'172.0','59.6','58.5','65.1','71.6','-5.5','18.0','13.5','18.5','10.7','8.8','12.0','0.70','0.73','1.87','1.91','5.47','33222','48.9','53.1','56.3','35.2','38.2','40.5','20.1','18.5','25.0','8.4','1488','560','10.2','10.4','11.7','3.5','3.7','3.9','45.4','49.2','52.4','2.81','2.73','8.08','7.81','23.97','11111','0.79','0.90','0.75','7','18','2292','59','1','79.6'),('2025-06-15 14:14:00',5,NULL,'156.0','55.9','48.1','53.5','58.8','2.4','28.1','19.5','29.5','15.7','10.4','15.8','0.97','0.96','2.83','2.83','8.11','22222','40.2','37.7','43.1','28.9','27.1','31.0','22.9','18.5','25.0','4.4','1237','524','8.1','7.4','8.5','3.2','2.9','3.2','37.0','34.7','39.8','2.48','2.49','6.79','6.79','18.45','22222','0.78','0.85','0.70','6','20','1905','41','5','74.4'),('2025-06-15 16:12:00',1,NULL,'190.0','95.6','71.4','79.4','87.3','16.2','24.5','15.0','20.0','23.4','11.9','15.9','1.53','1.57','4.13','4.20','11.95','33333','72.2','63.5','67.5','52.0','45.7','48.6','26.4','18.5','25.0','20.4','1775','421','14.7','12.7','14.2','5.5','4.6','4.7','66.7','58.9','62.7','4.34','4.23','12.38','12.09','33.66','33333','0.90','0.90','0.75','11','40','2734','103','19','97.7'),('2026-06-15 12:13:00',12,NULL,'165.0','58.8','53.9','59.9','65.8','-1.1','16.0','15.0','20.0','9.4','9.0','12.0','0.62','0.63','1.65','1.69','4.80','22222','49.4','47.9','50.9','35.6','34.4','36.6','21.5','18.5','25.0','1.8','1432','417','10.4','9.5','10.7','3.4','3.4','3.5','46.0','44.4','47.3','2.94','2.88','8.49','8.29','23.40','21222','0.78','0.90','0.75','6','21','2205','54','0','77.7');
/*!40000 ALTER TABLE `ioi353` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nada`
--

DROP TABLE IF EXISTS `nada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nada` (
  `val_FechaMuestra` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nada`
--

LOCK TABLES `nada` WRITE;
/*!40000 ALTER TABLE `nada` DISABLE KEYS */;
INSERT INTO `nada` VALUES ('2009-03-15 11:06:00'),('2009-03-15 11:06:00');
/*!40000 ALTER TABLE `nada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacdoc`
--

DROP TABLE IF EXISTS `pacdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacdoc` (
  `id_PacDoc` int(11) NOT NULL AUTO_INCREMENT,
  `Nombres` varchar(100) NOT NULL,
  `Apellidos` varchar(100) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `eMail` varchar(127) NOT NULL,
  `Gender` char(1) NOT NULL,
  `esDoctor` tinyint(1) DEFAULT '0',
  `lastHeight` float DEFAULT NULL,
  `lastWeight` float DEFAULT NULL,
  `lastReading` datetime DEFAULT NULL,
  `Clave` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_PacDoc`),
  UNIQUE KEY `eMail` (`eMail`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacdoc`
--

LOCK TABLES `pacdoc` WRITE;
/*!40000 ALTER TABLE `pacdoc` DISABLE KEYS */;
INSERT INTO `pacdoc` VALUES (1,'Tomás Emmanuel','Higareda Pliego','1977-04-16','thigareda@hotmail.com','H',0,NULL,NULL,NULL,NULL),(2,'Juan','Torres','2012-01-01','juan@chiquito.com','H',0,NULL,NULL,NULL,NULL),(3,'Pedro','Campos','2003-03-01','Pedro@isgay.com','H',0,NULL,NULL,NULL,NULL),(4,'Jose','Perez','1993-03-01','jose@hotmail.com','H',0,NULL,NULL,NULL,NULL),(5,'MICHELLE DE JESUS','SANCHEZ MAULEON','1995-06-16','michelle.mauleon@hotmail.com','M',0,NULL,NULL,NULL,NULL),(6,'Deyber Erickson','Rodriguez Alvarez','1991-10-19','derodriguez_00@hotmail.com','H',0,NULL,NULL,NULL,NULL),(7,'Evelyn ','Vega Garcia','1982-07-09','evelyn.vega@edenred.com','M',0,NULL,NULL,NULL,NULL),(8,'Sergio Jesus','Ayala Huicochea','1993-10-08','sergio.jesus.ayala@gmail.com','H',0,NULL,NULL,NULL,NULL),(9,'Jose Antonio ','Ortiz Espinosa','1997-01-16','pepe_493@hotmail.com','H',0,NULL,NULL,NULL,NULL),(10,'Luis Enrique','Garcia Garcia','1992-02-11','quique_735@hotmail.com','H',0,NULL,NULL,NULL,NULL),(11,'Christian Eduardo','Lopez Lopez','1990-02-24','lctgrk@gmail.com','H',0,NULL,NULL,NULL,NULL),(12,'Uriel','Diaz Ocampo','1993-04-04','uriel_guardadof10@hotmail.com','H',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pacdoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `r_pacdoc`
--

DROP TABLE IF EXISTS `r_pacdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `r_pacdoc` (
  `id_paciente` int(11) NOT NULL DEFAULT '0',
  `id_doctor` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_paciente`,`id_doctor`),
  KEY `id_doctor` (`id_doctor`),
  CONSTRAINT `r_pacdoc_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacdoc` (`id_PacDoc`),
  CONSTRAINT `r_pacdoc_ibfk_2` FOREIGN KEY (`id_doctor`) REFERENCES `pacdoc` (`id_PacDoc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `r_pacdoc`
--

LOCK TABLES `r_pacdoc` WRITE;
/*!40000 ALTER TABLE `r_pacdoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `r_pacdoc` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-01 14:19:19
